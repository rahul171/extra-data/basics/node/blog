const { Schema } = require('mongoose');

const schema = new Schema({
    title: {
        type: String,
        required: true,
    },
    content: {
        type: String,
        required: true
    },
    author: {
        type: String
    },
    tags: {
        type: Array
    }
});

module.exports = schema;
