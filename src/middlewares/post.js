const router = require('express').Router();
const error = require('./middlewares/error');

router.use(error);

module.exports = router;
