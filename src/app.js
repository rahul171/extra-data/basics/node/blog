const express = require('express');
const app = express();

const connection = require('./db/mongoose');
connection.connect();

const middleware = require('./middlewares');
const routes = require('./routes');

middleware(app).preRegister();
routes.register(app);
// somehow post registered error handler middleware is not working.
// TODO: debug the issue.
// middleware(app).postRegister();

const messageController = require('./controllers/message');

app.use((err, req, res, next) => {
    res.status(400)
        .send(messageController.getErrorResponse(err.message));
    next();
});

module.exports = app;
