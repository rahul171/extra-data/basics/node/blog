const model = require('../models/post');

const exists = async (id) => {
    const post = await model.getById(id);
    return !!post;
}

const addPost = (post) => {
    return model.add(post);
}

const updatePost = async (id, post) => {
    const exist = await exists(id);
    if (!exist) {
        throw new Error(`Post with id: ${id} doesn't exists`);
    }
    return model.update(id, post);
}

const deletePost = async (id) => {
    const exist = await exists(id);
    if (!exist) {
        throw new Error(`Post with id: ${id} doesn't exists`);
    }
    return model.remove(id);
}

const getPosts = () => {
    return model.getAll();
}

const getPost = async (id) => {
    const exist = await exists(id);
    if (!exist) {
        throw new Error(`Post with id: ${id} doesn't exists`);
    }
    return model.getById(id);
}

module.exports = { addPost, updatePost, deletePost, getPosts, getPost };
