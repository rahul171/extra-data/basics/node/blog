const router = require('express').Router();
const requestDataParser = require('./middlewares/requestDataParser');
const cors = require('./middlewares/cors');

router.use(requestDataParser);
router.use(cors);

module.exports = router;
