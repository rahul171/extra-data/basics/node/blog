const router = require('express').Router();
const controller = require('../controllers/post');
const messageController = require('../controllers/message');

router.get('/', async (req, res, next) => {
    try {
        const posts = await controller.getPosts();
        res.send(posts);
    } catch (err) {
        return next(err);
    }
});

router.get('/:id', async (req, res, next) => {
    try {
        const post = await controller.getPost(req.params.id);
        res.send(post);
    } catch (err) {
        return next(err);
    }
});

router.post('/', async (req, res, next) => {
    try {
        const post = await controller.addPost(req.body);
    } catch (err) {
        return next(err);
    }
    res.send(messageController.getSuccessResponse('post added successfully'));
});

router.patch('/', async (req, res, next) => {
    try {
        const response = await controller.updatePost(req.body);
    } catch (err) {
        return next(err);
    }
    res.send(messageController.getSuccessResponse('post updated successfully'));
});

router.delete('/:id', async (req, res, next) => {
    try {
        const deleted = await controller.deletePost(req.params.id);
    } catch (err) {
        return next(err);
    }
    res.send(messageController.getSuccessResponse('post deleted successfully'));
});

module.exports = router;
