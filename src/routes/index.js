const home = require('./home');
const post = require('./post');

const register = (app) => {
    app.use('/', home);
    app.use('/posts', post);
    app.use('/postserr', (req, res) => {
        res.status(400).send({error: 'something went wrong'})
    });
}

module.exports = { register };
