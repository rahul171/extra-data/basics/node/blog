const router = require('express').Router();
const loggerService = require('../../services/logger');
const messageController = require('../../controllers/message');

router.use((err, req, res, next) => {
    loggerService.log('here');
    loggerService.log(err);
    loggerService.log(err.message);
    res.status(400)
        .send(messageController.getErrorResponse(err.message));
    next();
});

module.exports = router;
