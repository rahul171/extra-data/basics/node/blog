const router = require('express').Router();
const cors = require('cors');

router.use(cors());

module.exports = router;
