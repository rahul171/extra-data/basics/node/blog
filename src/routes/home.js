const router = require('express').Router();

router.get('/', (req, res, next) => {
    res.send('home');
});

module.exports = router;
