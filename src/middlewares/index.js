const pre = require('./pre');
const post = require('./post');

const middleware = (app) => {
    return {
        preRegister: () => {
            app.use(pre);
        },
        postRegister: () => {
            app.use(post);
        }
    };
}

module.exports = middleware;
