const getSuccessResponse = message => ({ message });

const getErrorResponse = error => ({ error });

module.exports = { getSuccessResponse, getErrorResponse };
