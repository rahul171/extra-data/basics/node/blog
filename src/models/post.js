const mongoose = require('mongoose');
const schema = require('./schemas/post');

const Model = mongoose.model('Post', schema);

const add = (postData) => {
    const post = new Model(postData);
    return post.save();
}

const get = (id) => {
    if (!id) {
        return getAll();
    }

    return getById(id);
}

const getAll = () => {
    return Model.find({});
}

const getById = (id) => {
    return Model.findById(id);
}

const update = (id, post) => {
    return Model.updateOne({ _id: id }, post, { runValidators: true });
}

const remove = (id) => {
    return Model.deleteOne({ _id: id });
}

module.exports = { add, update, remove, getAll, getById, Model };
