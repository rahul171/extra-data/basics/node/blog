// validation of request data should be done here.

const postService = require('../services/post');

const addPost = (data) => {
    return postService.addPost(data);
}

const updatePost = async ({ _id: id, ...data }) => {
    return postService.updatePost(id, data);
}

const deletePost = (id) => {
    return postService.deletePost(id);
}

const getPosts = () => {
    return postService.getPosts();
}

const getPost = (id) => {
    return postService.getPost(id);
}

module.exports = { addPost, updatePost, deletePost, getPosts, getPost };
